package utng.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utng.dom.ProductDOM;
import utng.model.Product;

/**
 * Servlet implementation class ProductController
 */
@WebServlet("/ProductController")
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Product product;
	private List<Product> products;
	private ProductDOM productDOM;
	private List<String> ids = new ArrayList<String>();    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductController() {
        super();
        product = new Product();
        products = new java.util.ArrayList<>();
        productDOM = new ProductDOM();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		if (request.getParameter("btn_save") != null) {
			product.setSymbol(request.getParameter("symbol"));
				//System.out.println("Save");
			try {
				product.setSpecies(Integer.parseInt(request.getParameter("species")));
				product.setType(Integer.parseInt(request.getParameter("type")));
			} catch (NumberFormatException e) {
				product.setSpecies(8);
				product.setType(10);
			}
			product.setName(request.getParameter("name"));
			System.out.println("Save");
			if (product.getId() == "") {
				String newId =  "pdt"+String.format("%05d", 1);
				product.setId(newId);
				if (products.size()>0) {
					ids.clear();
					for(Product s: products) {
						ids.add(s.getId());
					}
					for (int i=0; i<=ids.size(); i++) {
						newId = "pdt"+String.format("%05d", i+1);
						if (!ids.contains(newId)) {
							product.setId(newId);
							break;
						}
					}
				}
				productDOM.add(product);

			} else {
				productDOM.update(product);

			}
			products = productDOM.getSProducts();
			request.setAttribute("products", products);
			request.getRequestDispatcher("product_list.jsp").forward(request, response);
			
		}else if(request.getParameter("btn_new")!=null) {
			product = new Product();
			request.getRequestDispatcher("product_form.jsp").forward(request, response);
		}else if(request.getParameter("btn_edit")!=null) {
			try {
				String id = request.getParameter("id");
				product = productDOM.findById(id);
			}catch (IndexOutOfBoundsException e) {
				product = new Product();
			}
		 request.setAttribute("product", product);
		 request.getRequestDispatcher("product_form.jsp").forward(request, response);
		}else if(request.getParameter("btn_delete")!=null) {
			try {
				String id= request.getParameter("id");
				productDOM.delete(id);
				products = productDOM.getSProducts();
		
			}catch (Exception e) {
				e.printStackTrace();
			}
			request.setAttribute("products", products);
			request.getRequestDispatcher("product_list.jsp").forward(request, response);
		}else {
			products = productDOM.getSProducts();
			request.setAttribute("products", products);
			request.getRequestDispatcher("product_list.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
