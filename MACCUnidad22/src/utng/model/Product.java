/**-
 * 
 */
package utng.model;

import java.io.Serializable;

/**
 * @author usuario
 *
 */
public class Product implements Serializable {
	private String id;
	private String Symbol;
	private int Species;
	private int Type;
	private String name;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return Symbol;
	}
	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		Symbol = symbol;
	}
	/**
	 * @return the species
	 */
	public int getSpecies() {
		return Species;
	}
	/**
	 * @param species the species to set
	 */
	public void setSpecies(int species) {
		Species = species;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return Type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		Type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public Product(String id, String symbol, int species, int type, String name) {
		super();
		this.id = id;
		Symbol = symbol;
		Species = species;
		Type = type;
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [id=" + id + ", Symbol=" + Symbol + ", Species=" + Species + ", Type=" + Type + ", name=" + name
				+ "]";
	}
	
	public Product() {
		this("","",0,0,"");
	}
	
	
}
